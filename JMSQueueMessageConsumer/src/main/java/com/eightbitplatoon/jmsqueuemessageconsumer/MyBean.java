package com.eightbitplatoon.jmsqueuemessageconsumer;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Benny
 *
 */
public class MyBean implements Processor {

	/**
	 * Class logger.
	 */
	private static Logger log = LoggerFactory.getLogger(MyBean.class);

	/* (non-Javadoc)
	 * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
	 */
	@Override
	public void process(final Exchange exchange) throws Exception {
		String msg = exchange.getIn().getBody().toString();
		log.info(msg);
	}
}
