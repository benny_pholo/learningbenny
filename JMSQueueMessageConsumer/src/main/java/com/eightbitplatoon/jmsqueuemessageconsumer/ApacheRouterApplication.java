package com.eightbitplatoon.jmsqueuemessageconsumer;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * @author Benny
 *
 */
@SpringBootApplication
@ImportResource("classpath:Beans.xml")
public class ApacheRouterApplication {

}
