package com.eightbitplatoon.JMSQueueMessageConsumer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import com.eightbitplatoon.jmsqueuemessageconsumer.ApacheRouterApplication;

/**
 * @author Benny
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Import(ApacheRouterApplication.class)
@SpringBootConfiguration
public class ApacheRouterApplicationTests {
	/**
	 * Loading Camel Context.
	 */
	@Test
	public void contextLoads() {
	}
}
