#MTN VAS-DISPLAY Automation Application#
### What is this repository for? ###

* Quick summary
This is a in-store automated test application using serenity, maven as our build tool, and jenkins for running tests
and deploying to test environment.

### How do I get set up? ###

* Summary of set up
Clone the project from GIT REPO
Download chrome Webdriver https://sites.google.com/a/chromium.org/chromedriver/downloads
Extract the zip file to the application path (this should be an .exe file not the folder)


* Configuration
Property files for PROD, Test - This contains all the configuration for different environments.
Property file for serenity - This contains serenity test root, timeout stamp, browser width and height and many more.

* How to run tests
Uncomment last chrome.switches configuration on the serenity.properties (Remember to comment it back once the test are done)
Open console on the application path.
Type "mvn clean verify -Dcontext=chrome -Denv=test -Ptablet" this will run all the tests on your local machine
For remote (No need to comment the chrome.switches)
mvn clean verify -Dcontext=chrome -Dwebdriver.remote.url=http://selenium-hub.apps.ops.test-digital.cellc.services/wd/hub -Dwebdriver.remote.driver=chrome -Denv=test -Ptablet
Running tests with specific annotations
mvn clean verify -Dcontext=chrome -Denv=test -Ptablet -Dtags="{tagName}"
eg:
mvn clean verify -Dcontext=chrome -Denv=test -Ptablet -Dtags="Transaction"


* Deployment instructions
Jenkins is used to automatically deploy to UAT environment once the code is pushed via git and all the test are passing

### Contribution guidelines ###

* Writing tests
Developers, BA and Testers
* Code review
Testers and Developers

### Who do I talk to? ###

* Repo owner or admin
Lerato, Benny and
* Other community or team contact
MTN VAS-Display Team lead