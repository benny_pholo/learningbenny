package za.co.mtn.util;

import lombok.extern.slf4j.Slf4j;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.apache.commons.lang3.StringUtils;
import org.yaml.snakeyaml.Yaml;

import java.util.Map;
import java.util.Properties;

@Slf4j
public class EnvironmentProperties {

    private static Properties properties;

    public static String getProp(String key) throws Exception {
        if (properties == null) {
            initProperties();
        }
        return properties.getProperty(key);
    }

    private static Properties readProperties(String profile) throws Exception{
        String defaultFile = "environment-config.yml";
        String fileName = defaultFile;
        if(StringUtils.isNotEmpty(profile)){
            fileName = "environment-config-".concat(profile).concat(".yml");
        }

        if(EnvironmentProperties.class.getResource("/".concat(fileName))==null){
            fileName =defaultFile;
        }
        Yaml yaml = new Yaml();
        Map<String,Object> map = yaml.load(EnvironmentProperties.class.getClassLoader().getResourceAsStream(fileName));
        Properties properties = new Properties();
        return  addToProps(map, properties,"");
    }

    private static void initProperties() throws Exception{
        EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
        String profile = variables.getProperty("env");
        properties = readProperties(profile);
    }

    private static Properties addToProps(Map<String, Object> map, Properties props, String prefix) {
        for (Map.Entry<String, Object> entrySet : map.entrySet()) {
            String key = entrySet.getKey();
            String propKey = generateKey(key, prefix);
            if (entrySet.getValue() instanceof Map) {
                addToProps((Map<String, Object>) entrySet.getValue(), props, propKey);
            } else {
                props.put(propKey, entrySet.getValue().toString());
            }

        }
        return props;
    }

    private static String generateKey(String key, String prefix) {
        String response;
        if (StringUtils.isNotEmpty(prefix)) {
            response = prefix.concat(".").concat(key);
        } else {
            response = key;
        }
        return response;
    }


}
