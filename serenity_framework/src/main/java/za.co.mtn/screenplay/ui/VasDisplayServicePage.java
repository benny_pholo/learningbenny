package za.co.mtn.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Question;
import net.thucydides.core.pages.PageObject;
import org.junit.Assert;

import java.util.List;

public class VasDisplayServicePage extends PageObject {


    // Login as CSR Super administrator then search subscriber by cellphone number and login in as Account holder/Subscriber
    public void verifyOverviewScreenCsrSuperAdministrator(List<String> fields) {
        for (int i=0; i<fields.size();i++){
            String expectedField = fields.get(i);
            String actualField = getDriver().findElement(By.linkText(expectedField)).getText();
            Assert.assertEquals(actualField, expectedField);
        }
    }


}
