package za.co.mtn.screenplay.ui;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.pages.PageObject;


public class LoginPage extends PageObject {

    public static final Target USERNAME = Target.the("Username").locatedBy("#username");
    public static final Target SUBMIT_BUTTON = Target.the("login submit button").locatedBy("#submitButton");
    public static final Target DASHBOARD_LABEL = Target.the("Dashboard label").locatedBy("//h1[text()='Overview']") ;

    public static Question<Boolean> dashboardLabel(){
        return actor -> LoginPage.DASHBOARD_LABEL.resolveFor(actor).isVisible();
    }
}
