package za.co.mtn.screenplay.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class OpenBrowser implements Task{

    private String url;

    public OpenBrowser(String url){this.url=url;}

    @Override
    @Step("{0} Open the browser #url")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.url(url)
        );
    }

    public static Performable andGoToUrl(String url){
        return instrumented(OpenBrowser.class,url);
    }
}
