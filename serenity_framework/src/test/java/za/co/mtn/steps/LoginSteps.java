package za.co.mtn.steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import za.co.mtn.screenplay.tasks.OpenBrowser;
import za.co.mtn.screenplay.ui.LoginPage;
import za.co.mtn.util.EnvironmentProperties;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class LoginSteps {

    @Before
    public void set_the_stage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^I have prepared to test in the default configured environment$")
    public void iHavePreparedToTestInTheDefaultConfiguredEnvironment() throws Throwable{
        theActorCalled("Test").attemptsTo(OpenBrowser.andGoToUrl(
                EnvironmentProperties.getProp("baseUrl")
        ));
    }

    @When("^The user login with a valid CSR Super administrator credentials$")
    public void theUserLoginWithAValidCSRSuperAdministratorCredentials() throws Throwable{
        theActorInTheSpotlight().attemptsTo(
                Enter.theValue(EnvironmentProperties.getProp("CSR_SUPER_ADMINISTRATOR")).into(LoginPage.USERNAME),
                Click.on(LoginPage.SUBMIT_BUTTON)
        );
    }

    @Then("^Land on the overview screen$")
    public void landOnTheOverviewScreen() {
        theActorInTheSpotlight().should(
                seeThat(" the dashboard label is displayed",LoginPage.dashboardLabel())
        );
    }

    @When("^The user login with a valid Corporate Super administrator credentials$")
    public void theUserLoginWithAValidCorporateSuperAdministratorCredentials() throws Throwable {
        theActorInTheSpotlight().attemptsTo(
                Enter.theValue(EnvironmentProperties.getProp("CORPORATE_SUPER_ADMIN")).into(LoginPage.USERNAME),
                Click.on(LoginPage.SUBMIT_BUTTON)
        );
    }

    @When("^The user login with a valid Consumer Account holder credentials$")
    public void theUserLoginWithAValidConsumerAccountHolderCredentials() throws Throwable {
        theActorInTheSpotlight().attemptsTo(
                Enter.theValue(EnvironmentProperties.getProp("DEFAULT_ACCOUNT_HOLDER_CONSUMER")).into(LoginPage.USERNAME),
                Click.on(LoginPage.SUBMIT_BUTTON)
        );
    }

    @When("^The user login with a valid Dealer BRC CSR credentials$")
    public void theUserLoginWithAValidDealerBRCCSRCredentials() throws Throwable {
        theActorInTheSpotlight().attemptsTo(
                Enter.theValue(EnvironmentProperties.getProp("DEALER_BRC_CSR")).into(LoginPage.USERNAME),
                Click.on(LoginPage.SUBMIT_BUTTON)
        );
    }
}
