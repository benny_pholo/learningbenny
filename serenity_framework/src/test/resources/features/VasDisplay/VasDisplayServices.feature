Feature: As an MTN user I want to be able to login with different profiles and display vas services

  Background: Open MTN website
    Given I have prepared to test in the default configured environment
    

	@regression
  Scenario: Login as CSR Super administrator then search subscriber by cellphone number and login in as Account holder and verify vas display services page.
    When The user login with a valid CSR Super administrator credentials
    Then Land on the CSR Super administrator overview screen with the icons "Add organisation,Search organisations,Search subscribers,User events,Transaction events"
#    When I click on Search subscribers link
#    Then Organisation search screen displayed with the fields "Cellphone number,Account number,SIM number,South African ID number"
#    When I Select search type "Cellphone number" enter value "0827073623" and submit
#    And I verify table fields "Name,Cellphone number,Account number,South African ID number,Login,PUK information" and click on Login link
#    Then Subscriber details screen displayed with the fields "Name,Cellphone number,Subscriber ID number,Role,Address 1,Address 2,Address 3,City / Town,Postal code,Legal entity name,Customer type,Account status,South African ID number,One key question,Answer,One key question answered correctly?"
#    And I checked One key question answered correctly checkbox
#    When I submit Authentication form
#    Then Land on the overview homepage with the icons "Upgrade subscriber,Migrate subscriber,Suspend SIM,Reactivate SIM,Activate services,Deactivate services,Request itemised bill,View statements"
#    When I click "Display services" link from My Services tab
#    Then Display services screen displayed with all "Mobile standard services,Mobile optional services,Current ICT services" services
#
#
#  @regression
#  Scenario: Login as CSR Super administrator then search subscriber by cellphone number and login in as Subscriber and verify vas display services page.
#		When The user login with a valid CSR Super administrator credentials
#    Then Land on the CSR Super administrator overview screen with the icons "Add organisation,Search organisations,Search subscribers,User events,Transaction events"
#    When I click on Search subscribers link
#    Then Organisation search screen displayed with the fields "Cellphone number,Account number,SIM number,South African ID number"
#    When I Select search type "Cellphone number" enter value "0837798118" and submit
#    And I verify table fields "Name,Cellphone number,Account number,South African ID number,Login,PUK information" and click on Login link
#    Then Subscriber details screen displayed with the fields "Name,Cellphone number,Subscriber ID number,Role,Address 1,Address 2,Address 3,City / Town,Postal code,Legal entity name,Primary MSISDN,Customer type,Account status,South African ID number"
#    And I checked Authentication checkbox
#    When I submit Authentication form
#    Then Land on the overview homepage with the icons "Upgrade subscriber,Migrate subscriber,Request itemised bill,View invoices,Go to account holder"
#    When I click "Display services" link from My Services tab
#    Then Display services screen displayed with all "Mobile standard services,Mobile optional services,Current ICT services" services
#
#
#
#  @regression
#  Scenario: Login as Corporate Super administrator then search subscriber by cellphone number and verify vas display services page.
#		When The user login with a valid Corporate Super administrator credentials
#    Then Land on the Corporate Super administrator overview screen with the icons "Create a Remedy Ticket,Modify bill delivery method,Search subscribers,View billing reports,View hierarchy,View statements,View TopX reports"
#    When I click on Search subscribers link
#    Then Subscriber details screen displayed with the fields "First name,Last name,Cellphone number,All levels below?"
#    When I search with "Cellphone number" enter value "0634073591" and submit
#    And I verify table fields "Name,Level,Cellphone number,Login,PUK information,SIM history" and click on Login link
#    Then Land on the overview homepage with the icons "SIM swap,Activate services,View invoices,View PUK numbers,Update details"
#    When I click "Display services" link from My Services tab
#    Then Display services screen displayed with all "Mobile standard services,Mobile optional services,Current ICT services" services
#
#  @regression
#   Scenario: Login as Consumer Account holder and verify vas display services page.
#   	When The user login with a valid Consumer Account holder credentials
#    Then Land on the Consumer Account holder overview screen with the icons "View my statements,View my unbilled invoice,View my upgrade eligibility,Activate a service,Deactivate a service,Update my details,View my PUK numbers,Modify bill delivery method,View my data usage statistics"
#    When I click "Display services" link from My Services tab
#    Then Display services screen displayed with all "Mobile standard services,Mobile optional services,Current ICT services" services
#
#  @regression
#  Scenario: Login as Dealer (BRS CSR). Search subscriber by cellphone number and login in as Account holder and verify vas display services page.
#  	When The user login with a valid Dealer BRC CSR credentials
#  	Then Land on the Dealer BRS CSR overview screen with the icons "Search subscribers,Transaction events,User events"
#  	When I click on Search subscribers link
#  	Then Organisation search screen displayed with the fields "Cellphone number,Account number,SIM number,South African ID number"
#  	When I Select search type "Cellphone number" enter value "0837818324" and submit
#  	And I verify table fields "Name,Cellphone number,Role,Account holder,Login,PUK information" and click on Login link
#		Then Subscriber details screen displayed with the fields "First name,Last name,Cellphone number,South African ID number,Role,Address 1,Address 2,Address 3,City / Town,Postal code,Legal entity name,Customer type,Account status,Business registration number"
#  	And I checked Authentication checkboxs
#  	When I submit Authentication form
#		Then Land on the overview homepage with the icons "Upgrade subscriber,Migrate subscriber,Suspend SIM,Reactivate SIM,SIM swap,Activate services,Deactivate services,Request itemised bill,View statements,Document upload"
#		When I click "Display services" link from My Services tab
#		Then Display services screen displayed with all "Mobile standard services,Mobile optional services,Current ICT services" services
#
#
#   #================NEGATIVE TESTING====================
#   @regression
#   Scenario: Login as Dealer (BRS CSR). Search subscriber by invalid cellphone number and verify search result for negative schenario.
#    When The user login with a valid Dealer BRC CSR credentials
#  	Then Land on the Dealer BRS CSR overview screen with the icons "Search subscribers,Transaction events,User events"
#  	When I click on Search subscribers link
#  	Then Organisation search screen displayed with the fields "Cellphone number,Account number,SIM number,South African ID number"
#  	When I Enter cellphone number "0837818366" and submit
#  	Then Search result should display "Your search returned no results"
#  	#When I switch to "Account number" on the search type
#  	#Then Verify text box "Account number" should be blank
#
#  	@regression
#  Scenario: Login as CSR Super administrator then search subscriber by cellphone number and login in as Account holder for negative schenario.
#  	 When The user login with a valid CSR Super administrator credentials
#    Then Land on the CSR Super administrator overview screen with the icons "Add organisation,Search organisations,Search subscribers,User events,Transaction events"
#    When I click on Search subscribers link
#    Then Organisation search screen displayed with the fields "Cellphone number,Account number,SIM number,South African ID number"
#    When I Select search type "Cellphone number" enter value "0827073623" and submit
#    And I verify table fields "Name,Cellphone number,Account number,South African ID number,Login,PUK information" and click on Login link
#    Then Subscriber details screen displayed with the fields "Name,Cellphone number,Subscriber ID number,Role,Address 1,Address 2,Address 3,City / Town,Postal code,Legal entity name,Customer type,Account status,South African ID number,One key question,Answer,One key question answered correctly?"
#    And I checked One key question answered correctly checkbox
#    When I submit Authentication form
#    Then Land on the overview homepage with the icons "Upgrade subscriber,Migrate subscriber,Suspend SIM,Reactivate SIM,Activate services,Deactivate services,Request itemised bill,View statements"
#    When I click "Deactivate a mobile service" link from My Services tab
  	