Feature: Login Action


  Background:
  	Given I have prepared to test in the default configured environment
    #Given Open mtn website "https://mycontract-uat1.mtn.co.za/landing/landing.htm"
    

  @SmokeTest
  Scenario: Successful Login as CSR Super administrator.
  	When The user login with a valid CSR Super administrator credentials
    Then Land on the overview screen

  @SmokeTest
  Scenario: Successful Login as Corporate Super administrator.
    When The user login with a valid Corporate Super administrator credentials
    Then Land on the overview screen



  @SmokeTest
  Scenario: Successful Login with Consumer Account holder.
    When The user login with a valid Consumer Account holder credentials
    Then Land on the overview screen


  @SmokeTest
  Scenario: Successful Login with Dealer BRC CSR.
    When The user login with a valid Dealer BRC CSR credentials
    Then Land on the overview screen

