.. Learning Maven

Maven Information
=================
Create an AEM Archetype 10 project
----------------------------------
To create an AEM archetype 10 project, perform these steps:

1. Open the command prompt and go to your working directory (for example, C:\AdobeCQ).

2. Run the following Maven command:
 
     .. AEM + Maven code blog :: Maven

    mvn archetype:generate -DarchetypeGroupId=com.adobe.granite.archetypes -DarchetypeArtifactId=aem-project-archetype -DarchetypeVersion=10 -DarchetypeRepository=https://repo.adobe.com/nexus/content/groups/public/

3. When prompted, specify the following information:  
	* groupId - Base Maven groupId(will identify your project uniquely across all projects, so we need to enforce a naming schema. It has to follow the package name rules, what means that has to be at least as a domain name you control, and you can create as many subgroups as you want-eg. org.apache.commons)
	* artifactId - Base Maven ArtifactId (is the name of the jar without version. If you created it then you can choose whatever name you want with lowercase letters and no strange symbols. If it's a third party jar you have to take the name of the jar as it's distributed- eg commons-math)
	* version - the version of your project
	* package - Java Source Package
	* appsFolderName - /apps folder name
	* artifactName - Maven Project Name
	* componentGroupName - AEM component group name
	* contentFolderName - /content folder name
	* cssId - prefix used in generated css
	* packageGroup - Content Package Group name
	* siteName - AEM site name
4. WHen prompted, specify Y.
5. Convert your project to eclipse specific.
	* mvn eclipse:eclipse


Fixing dependency issue for AEM 6.2
-----------------------------------
* Problem

In some cases when you use the AEM archetype 10 project and deploy it to AEM 6.2 you bundle doesnt get installed due to "javax.inject,version=[0.0,1) -- Cannot be resolved" issue, 

* Solution 

On the core pom file include the below in your apache felix plugin under instruction node:: 

 .. code-block:: pom file
	<Import-Package>
			javax.inject;version=0.0.0,
			com.day.cq.replication;version="[6.0.0,7)",
			org.apache.sling.event.jobs;version="[1.5,2]",
			com.day.cq.workflow;version="[1.0,2)",
			com.day.cq.workflow.exec;version="[1.0,2)",
			com.day.cq.workflow.metadata;version="[1.0,2)",
			org.apache.sling.models.annotations;version="[1.1,2)",
			com.day.cq.commons.jcr;version="[5.7.0,7.0.0)",
			*
	</Import-Package>  

The <Import-Package> instruction is a list of packages that are required by the bundle's contained packages. The default for this header is "*", resulting in importing all referred packages.



