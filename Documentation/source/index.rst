.. LearningBenny documentation master file, created by
   sphinx-quickstart on Fri Jun 09 15:43:10 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LearningBenny's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   aem/aem_index
   git/git_commands


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


